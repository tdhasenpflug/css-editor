(function ($) {
    var gset = {
        tab_length: 4,
    };

    function oo(o1, o2) {
        var o3 = {};
        for (var an in o1) {
            o3[an] = o1[an];
        }
        for (var an in o2) {
            o3[an] = o2[an];
        }
        return o3;
    }

    function tabl(n = gset.tab_length, v = ' ') {
        var text = '',
            i;
        for (i = 0; i < n; i++) {
            text += v;
        }
        return text;
    }

    function doTab(editor) {
        var text = editor.val();

        var ss = editor.prop('selectionStart'),
            se = editor.prop('selectionEnd');

        text = text.insert(ss, tabl());
        editor.val(text);
        editor.prop('selectionStart', ss + gset.tab_length);
        editor.prop('selectionEnd', se + gset.tab_length);
    }

    String.prototype.insert = function (index, value) {
        var st = this.slice(0, index),
            sa = value.toString(),
            se = this.slice(index, this.length);

        return st + sa + se;
    };

    String.prototype.replaceAll = function (search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };

    Array.prototype.clean = function () {
        for (var i = 0; i < this.length; i++) {
            if (this[i] == undefined) {
                this.splice(i, 1);
                i--;
            }
        }
        return this;
    };

    function tabLine(text) {
        text = text.trim();
        return tabl() + text;
    }

    function beautify(text) {
        text = text.replace(/([;\{\}])/g, '$1\n').replace(/ *(\{)/g, ' $1');

        var lines = text.split('\n');
        $.each(lines, function (n, e) {
            lines[n] = lines[n].trim();
            var ob = lines[n].indexOf('{'),
                cb = lines[n].indexOf('}');

            if (ob == '-1' && cb == '-1') {
                if (lines[n]) {
                    lines[n] = tabLine(lines[n]);
                }
            }

        });
        text = lines.clean().join('\n');
        text = text.replace(/\n+/g, '\n');

        return text;
    }
    
    function minify(text) {
        text = text.replace(/\n/g, '').replace(/ +/g, ' ').replace(/ *([,\}\{;:]) */g, '$1');
        return text;
    }

    var state = {
        shift: false,
        cmd: false,
    };

    function build_editor(settings = {}) {

        settings = oo({
            tab_length: 4,
            beautify_on_load: false,
            set_height_to_content: true,
            beautify_on_save: false,
            buttons: {
                beautify: true,
                color: true,
                minify: true,
                save: true,
            }
        }, settings);

        gset.tab_length = settings.tab_length;

        var editor_wrap = $(this),
            editor_content = editor_wrap.text();

        editor_wrap.html("<aside class='editor-toolbar'></aside><textarea class='editor-input' rows='10' spellcheck='false'></textarea>");

        var editor_tools = $(this).find('.editor-toolbar'),
            editor = $(this).find('.editor-input');

        if (settings.buttons.beautify) {
            editor_tools.append('<button class="beautify">Beautify <em>(cmd + b)</em></button>');
            editor_tools.find('.beautify').off().click(function (e) {
                e.preventDefault();
                editor.val(beautify(editor.val()));
            });
        }
        if (settings.buttons.minify) {
            editor_tools.append('<button class="minify">Minify <em>(cmd + m)</em></button>');
            editor_tools.find('.minify').off().click(function (e) {
                e.preventDefault();
                editor.val(minify(editor.val()));
            });
        }
        
//        if (settings.buttons.color) {
//            editor_tools.append('<button class="insert-color">Insert Color</button>');
//        }

        if (settings.buttons.save && settings.save && typeof settings.save == 'function') {
            editor_tools.append('<button class="editor-save">Save <em>(cmd + s)</em></button>');
            settings.buttons.save = editor_tools.find('.editor-save');
            settings.buttons.save.off().click(function (e) {
                e.preventDefault();
                settings.save(editor.val());
            });
        }

        if (settings.beautify_on_load === true) {
            editor_content = beautify(editor_content);
        }
        editor.val(editor_content);
        if (settings.set_height_to_content === true) {
            var rows = editor_content.split('\n').length;
            if (rows > 32) { rows = 32; }
            if (rows < 16) { rows = 16; }
            editor.attr('rows', rows);
        }

        editor.keydown(function (e) {
            switch (e.keyCode) {
            case 9:
                e.preventDefault();

                doTab(editor);

                break;
            case 91:
            case 93:
                /* Mac Command key */

                state.cmd = true;

                break;

            case 16:
                /* Shift key */

                state.shift = true;

                break;

            case 66:
                /* (b)eautify */
                if (state.cmd) {
                    e.preventDefault();
                    editor.val(beautify(editor.val()));
                }
                    break;
            case 83:
                /* (s)ave */
                if (state.cmd) {
                    e.preventDefault();
                    if (settings.save && typeof settings.save == 'function') {
                        settings.save(editor.val());
                    }
                }
                break;
            case 77:
                /* (m)inify */
                if (state.cmd) {
                    e.preventDefault();
                    editor.val(minify(editor.val()));
                }
                break;
            default:
                console.log(e.keyCode);
                break;
            }
        });
        editor.keyup(function (e) {
            switch (e.keyCode) {
            case 91:
                state.cmd = false;
                break;
            case 16:
                state.shift = false;
                break;
            default:
                break;
            }
        });
    }
    $.fn.extend({
        cssedit: function (settings) {
            return this.each(function () {
                build_editor.call(this, settings);
            });
        },
    });
})(jQuery);